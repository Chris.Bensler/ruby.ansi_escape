# -*- mode: ruby -*-
# vi: set ft=ruby :
# frozen_string_literal: true

# Copyright (c) 2016 Chris Bensler, All Rights Reserved

#################################################

require_relative 'ansi_escape/string_class'
require_relative 'ansi_escape/string_instance'

# String extension class
class String

  extend  AnsiEscape::StringClass
  include AnsiEscape::StringInstance

end
