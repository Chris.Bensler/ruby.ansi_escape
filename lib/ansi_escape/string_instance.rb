# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

module AnsiEscape

  # Extends String class
  module StringInstance

    def mode(symbol_or_code)
      code = symbol_to_code(symbol_or_code, self.class.modes)

      end_code = code
      end_code = end_code + 20  if code != self.class.modes[:default]
      end_code = end_code + 1   if code == self.class.modes[:bold]

      "\e[#{ code }m" + self + "\e[#{ end_code };0m"
    end

    def font(symbol_or_code)
      code     = 10 + symbol_to_code(symbol_or_code, self.class.fonts)
      end_code = 10 + self.class.fonts[:default]
      "\e[#{ code }m" + self + "\e[#{ end_code }m"
    end

    def fgcolor(symbol_or_code)
      code      = 30 + symbol_to_code(symbol_or_code, self.class.colors)
      end_code  = 30 + self.class.colors[:default]
      safe_code = 30 + self.class.colors[:white]
      "\e[#{ code }m" + self + "\e[#{ safe_code };#{ end_code }m"
    end

    def bgcolor(symbol_or_code)
      code      = 40 + symbol_to_code(symbol_or_code, self.class.colors)
      end_code  = 40 + self.class.colors[:default]
      safe_code = 40 + self.class.colors[:black]
      "\e[#{ code }m" + self + "\e[#{ safe_code };#{ end_code }m"
    end

    # alias for fgcolor()
    def color(symbol_or_code)
      fgcolor(symbol_or_code)
    end

    # alias for fgcolor()
    def foreground(symbol_or_code)
      fgcolor(symbol_or_code)
    end

    # alias for bgcolor()
    def background(symbol_or_code)
      bgcolor(symbol_or_code)
    end

    private

    def symbol_to_code(symbol_or_code, hash)
      self.class.symbol_to_code(symbol_or_code, hash)
    end

  end

end
