# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

module AnsiEscape

  # Extends String class
  module StringClass

    @@modes = {
      :default   => 0,
      :bold      => 1,
      :italic    => 3,
      :underline => 4,
      :blink     => 5,
      :invert    => 7,
      :hide      => 8,
      :strike    => 9,
    }

    @@fonts = {
      :default => 0,
      :font1   => 1,
      :font2   => 2,
      :font3   => 3,
      :font4   => 4,
      :font5   => 5,
      :font6   => 6,
      :font7   => 7,
      :font8   => 8,
      :font9   => 9,
    }

    @@colors = {
      :black   => 0,
      :red     => 1,
      :green   => 2,
      :yellow  => 3,
      :blue    => 4,
      :magenta => 5,
      :cyan    => 6,
      :white   => 7,
      :default => 9,
    }

    def modes
      @@modes
    end

    def fonts
      @@fonts
    end

    def colors
      @@colors
    end

    # lookup symbol in hash and convert to code
    def symbol_to_code(symbol_or_code, hash)
      is_sym = (symbol_or_code.is_a? Symbol)
      code = ( is_sym ) ? hash[symbol_or_code] : symbol_or_code

      if (code.nil? || !hash.value?(code)) then
        raise ArgumentError, get_symbol_to_code_error(symbol_or_code, hash)
      end

      return code
    end

    private

    def get_symbol_to_code_error(symbol_or_code, hash)
      typename = ( symbol_or_code.is_a? Symbol ) ? 'symbol' : 'code'
      symcode = "#{ ( typename == 'symbol' ) ? ':' : '' }#{ symbol_or_code }"

      errmsg = "Invalid #{ typename } :#{ symbol_or_code }\n"

      # self.class.indent += 1
      errmsg += "  #{ symcode } not found in hash\n"
      errmsg += pretty_hash(hash).gsub(/^/, '    ')
      errmsg += "\n"
      # self.class.indent -= 1

      return errmsg
    end

    def pretty_hash(hash)
      str = "{\n"

      # for alignment
      max = max_hash_symbol_length(hash) + 3

      # build the formatted hash string
      last = hash.keys[-1]
      hash.each do |symbol, code|
        str += "  :#{ symbol }".ljust(max)
        str += " => #{ code }#{ (symbol != last) ? ',' : '' }\n"
      end

      str += "}\n"
    end

    # find the max symbol length
    def max_hash_symbol_length(hash)
      max = 0

      hash.each_key do |symbol|
        size = symbol.to_s.size
        max = size    if size > max
      end

      return max
    end

  end

end
