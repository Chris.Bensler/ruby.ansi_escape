# -*- mode: ruby -*-
# vi: set ft=ruby :
# frozen_string_literal: true

# Copyright (c) 2016 Chris Bensler, All Rights Reserved

###############################################################################

require 'ansi_escape'

###############################################################################

String.modes.each_key do |symbol|
  print "#{ symbol }".mode( symbol ) + "\n"
end

String.fonts.each_key do |symbol|
  print "#{ symbol }".font( symbol ) + "\n"
end

String.colors.each_key do |symbol|
  print "#{ symbol }".color( symbol ) + "\n"
end

keys = String.colors.keys
keys.each do |fg|
  keys.each do |bg|
    txt = " #{ String.colors[fg] }:#{ String.colors[bg] }"
    print txt.fgcolor( fg ).bgcolor( bg )
  end
  print "\n"
end

print "\n"
print 'bold blue underlined'.mode(:bold).mode(:underline).fgcolor(:blue) + "\n"
print 'plain blue underlined'.mode(:underline).fgcolor(:blue) + "\n"

print "\e[0m \b"
