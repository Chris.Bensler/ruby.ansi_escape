# ansi_escape [![Gem Version](https://badge.fury.io/rb/ansi_escape.svg)](http://badge.fury.io/rb/ansi_escape)

# DESCRIPTION

Extends the String class to handle basic ANSI escape sequences without requiring any dependencies.

Supports:
 - format modes
 - fonts
 - colors


# REQUIREMENTS

None.


# INSTALLATION

````
gem install ansi_escape
````

Run `rake demo` to see examples.


# INTERFACE

`String.new.mode( symbol_or_code )`  
Apply a formatting mode to a *String* instance.  
***symbol_or_code*** can be any valid symbol or code from *String.modes*.  

`String.new.font( symbol_or_code )`  
Apply a font to a *String* instance.  
***symbol_or_code*** can be any valid symbol or code from *String.fonts*.  

`String.new.fgcolor( symbol_or_code )`  
Apply a foreground color to a *String* instance.  
***symbol_or_code*** can be any valid symbol or code from *String.colors*.  

`String.new.bgcolor( symbol_or_code )`  
Apply a background color to a *String* instance.  
***symbol_or_code*** can be any valid symbol or code from *String.colors*.  

`String.new.color( symbol_or_code )`  
Alias for *String.new.fgcolor*

`String.new.foreground( symbol_or_code )`  
Alias for *String.new.fgcolor*

`String.new.background( symbol_or_code )`  
Alias for *String.new.bgcolor*


# USAGE

```ruby
puts "bold".mode(:bold)
puts "italic".mode(:italic)
puts "underlined".mode(:underline)
puts "bold and italic".mode(:bold).mode(:italic)
puts "bold blue underlined".mode(:bold).mode(:underline).fgcolor(:blue)
puts "red on blue".fgcolor(:red).bgcolor(:blue)
```
# LICENSE

This software is released under the [MIT LICENSE](LICENSE).

Copyright (c) 2016 Chris Bensler
