# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

require 'rake/testtask'

namespace :test do
  desc 'Run unit tests'
  Rake::TestTask.new(:units) do |t|
    t.libs << 'lib'
    t.pattern = 'test/**/*_test.rb'
    t.verbose = false
  end
end

desc 'Run all unit_test tasks'
task :test => [
  'test:units',
]
