# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

require 'rubocop/rake_task'

namespace :validate do
  desc 'Sanitize Code'
  RuboCop::RakeTask.new(:clean) do |t|
    puts "\nPerforming Auto-Corrections"
    t.options << [
      # '--config', '.rubocop_auto.yml',
      # '--auto-gen-config',
      '--auto-correct',
      '--format', 'simple'
    ]
    t.options << [
      '--only', [
        'Style/Copyright',
        'Style/ColonMethodCall',
        'Style/EmptyLinesAroundBlockBody',
        'Style/FrozenStringLiteralComment',
        # 'Style/TrailingCommaInArguments',
        'Style/TrailingCommaInLiteral',
        'Style/TrailingWhitespace',
        'Style/FirstMethodArgumentLineBreak',
        'Style/FirstParameterIndentation',
        'Style/FirstArrayElementLineBreak',
        'Style/IndentArray',
        'Style/IndentHash',
        'Style/AlignHash',
        'Style/MultilineMethodCallBraceLayout',
        'Style/ClosingParenthesisIndentation',
        'Style/SpaceInsideHashLiteralBraces',
        'Style/TrailingBlankLines',
        'Style/StringLiterals',
        'Style/SpaceAfterComma',
        'Style/SpaceInsideBrackets',
      ].join(',')
    ]
  end
  Rake::Task['validate:clean:auto_correct'].clear

  desc 'Run RuboCop'
  RuboCop::RakeTask.new(:inspect) do |t|
    puts "\nInspecting Code"
    t.options << [
      # '--auto-gen-config',
      '--format', 'simple'
    ]
  end
  Rake::Task['validate:inspect:auto_correct'].clear
end

desc 'Run all validation tasks'
task :validate => [
  'validate:clean',
  'validate:inspect',
]
