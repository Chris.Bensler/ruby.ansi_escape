# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

namespace :demo do
  desc 'Run buffet demo'
  task :buffet do
    print `ruby -Ilib demo/buffet.rb`
  end
end

desc 'Run all demos'
task :demo => [
  'demo:buffet',
]
