# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

$LOAD_PATH.unshift File.expand_path('../lib', __FILE__)
require 'ansi_escape/version'

Gem::Specification.new do |s|
  s.name          = 'ansi_escape'
  s.version       = AnsiEscape::VERSION
  s.date          = Time.now.strftime('%Y-%m-%d')

  s.homepage      = 'https://gitlab.com/Chris.Bensler/ruby.ansi_escape'

  s.summary       = 'Extends the String class to handle ANSI escape sequences.'
  s.description   = <<-HERE
                    Extends the String class to handle basic ANSI escape sequences
                    without requiring any dependencies.
                    
                    Supports: format modes, fonts, colors
                    HERE

  s.license       = 'MIT'

  s.authors       = ['Chris Bensler']
  s.email         = ['chris.bensler@gmail.com']

  s.files         = `git ls-files app lib`.split("\n")
  s.require_paths = ['lib']
  s.platform      = Gem::Platform::RUBY
  
  s.required_ruby_version = '~> 2.0'

  s.add_development_dependency 'rake',     '~> 11.2'
  s.add_development_dependency 'rubocop',  '~> 0.42'
  s.add_development_dependency 'minitest', '~> 5.9'
end
