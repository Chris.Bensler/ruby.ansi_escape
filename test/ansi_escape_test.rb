# frozen_string_literal: true
# rubocop:disable Metrics/MethodLength

# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

require_relative 'test_helper'

class TestAnsiEscape < MiniTest::Test

  def test_modes_type
    assert_kind_of( Hash, String.modes )
  end

  def test_modes_value
    assert_equal(
      String.modes, { :default   => 0,
                      :bold      => 1,
                      :italic    => 3,
                      :underline => 4,
                      :blink     => 5,
                      :invert    => 7,
                      :hide      => 8,
                      :strike    => 9 }
    )
  end

  def test_fonts_type
    assert_kind_of( Hash, String.fonts )
  end

  def test_fonts_value
    assert_equal(
      String.fonts, { :default => 0,
                      :font1   => 1,
                      :font2   => 2,
                      :font3   => 3,
                      :font4   => 4,
                      :font5   => 5,
                      :font6   => 6,
                      :font7   => 7,
                      :font8   => 8,
                      :font9   => 9 }
    )
  end

  def test_colors_type
    assert_kind_of( Hash, String.colors )
  end

  def test_colors_value
    assert_equal(
      String.colors, { :black   => 0,
                       :red     => 1,
                       :green   => 2,
                       :yellow  => 3,
                       :blue    => 4,
                       :magenta => 5,
                       :cyan    => 6,
                       :white   => 7,
                       :default => 9 }
    )
  end

  # def test_pretty_hash
    # text = <<-EOF
      # {
        # :default   => 0,
        # :bold      => 1,
        # :italic    => 3,
        # :underline => 4,
        # :blink     => 5,
        # :invert    => 7,
        # :hide      => 8,
        # :strike    => 9
      # }
    # EOF
    # space = text.scan(/^[ \t]+/).min_by(&:length)
    # assert_equal(
      # text.gsub(/^#{ space }/, ''),
      # String.pretty_hash(String.modes)
    # )
  # end

  def test_symbol_to_code_pass
    [String.modes, String.fonts, String.colors].each do |hash|
      hash.each do |sym, code|
        assert_equal( code, String.symbol_to_code(sym, hash) )
      end
    end
  end

  def test_symbol_to_code_exception
    errmsg = <<-EOF
      Invalid symbol :bold
        :bold not found in hash
          {
            :black   => 0,
            :red     => 1,
            :green   => 2,
            :yellow  => 3,
            :blue    => 4,
            :magenta => 5,
            :cyan    => 6,
            :white   => 7,
            :default => 9
          }

    EOF
    space = errmsg.scan(/^[ \t]+/).min_by(&:length)
    errmsg = errmsg.gsub(/^#{ space }/, '')
    exception = assert_raises(ArgumentError) { 'invalid'.color(:bold) }
    assert_equal(errmsg, exception.message)
  end

end
