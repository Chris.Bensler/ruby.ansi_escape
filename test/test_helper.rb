# frozen_string_literal: true
# Copyright (c) 2016 Chris Bensler, All Rights Reserved.

require 'minitest/autorun'
# require 'minitest/reporters'
# Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require 'bundler/setup'

require 'ansi_escape'
